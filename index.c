#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>

void _mpi_loop(int targetRank, long max_ping_pong, MPI_Status status, int data_size)
{
	const int numberOfElements = data_size;
	const int dataValue = 999999999;
	long longTwo = 2;
	int*results = (int*)malloc(sizeof(int)*numberOfElements);
	long m1Scope = max_ping_pong / 2;
	double operationStart = MPI_Wtime();

	int*data = (int*)malloc(sizeof(int)*numberOfElements);
	for(int i = 0; i < numberOfElements; i++) {
		data[i] = dataValue;
	}

	if (targetRank %longTwo) {
		long startScope = 0;
		while(startScope < m1Scope) {
			MPI_Send(data, numberOfElements, MPI_INT, targetRank, 0, MPI_COMM_WORLD);
			for(int i = 0; i < numberOfElements; i++) {
				results[i] = 0;
			}
			MPI_Recv(results, numberOfElements, MPI_INT, targetRank, 0, MPI_COMM_WORLD, &status);
			++startScope;
		}
	} else {
		long startScope = m1Scope;
		while(startScope < max_ping_pong) {
			for(int i = 0; i < numberOfElements; i++) {
				results[i] = 0;
			}
			MPI_Recv(results, numberOfElements, MPI_INT, targetRank, 0, MPI_COMM_WORLD, &status);
			if (startScope >= max_ping_pong) continue; 
			MPI_Send(data, numberOfElements, MPI_INT, targetRank, 0, MPI_COMM_WORLD);
			
			++startScope;
		}
	}

	free(data);
	free(results);
}

void _mpi_loop_async(int targetRank, long max_ping_pong, int data_size)
{
	const int numberOfElements = data_size;
	const int dataValue = 999999999;
	MPI_Request *request = (MPI_Request *)malloc(max_ping_pong * sizeof(MPI_Request));
	MPI_Status *status = (MPI_Status *)malloc(max_ping_pong * sizeof(MPI_Status));

	long longTwo = 2;
	int*results = (int*)malloc(sizeof(int)*numberOfElements);
	long m1Scope = max_ping_pong / 2;
	double operationStart = MPI_Wtime();

	int*data = (int*)malloc(sizeof(int)*numberOfElements);
	for(int i = 0; i < numberOfElements; i++) {
		data[i] = dataValue;
	}
	if (targetRank % longTwo) {
		long startScope = m1Scope;
		while(startScope < max_ping_pong) {
			MPI_Isend(data, numberOfElements, MPI_INT, targetRank, 0, MPI_COMM_WORLD, &request[startScope]);
			++startScope;
		}
	} else {
		long startScope = 0;
		while(startScope < m1Scope) {
			MPI_Isend(data, numberOfElements, MPI_INT, targetRank, 0, MPI_COMM_WORLD, &request[startScope]);
			++startScope;
		}
	}

	if (targetRank % longTwo) {
		long startScope = 0;
		while(startScope < m1Scope) {
			MPI_Irecv(results, numberOfElements, MPI_INT, targetRank, 0, MPI_COMM_WORLD, &request[startScope]);
			MPI_Wait(&request[startScope], MPI_STATUS_IGNORE);
			++startScope;
		}
	} else {
		long startScope = m1Scope;
		while(startScope < max_ping_pong) {
			MPI_Irecv(results, numberOfElements, MPI_INT, targetRank, 0, MPI_COMM_WORLD, &request[startScope]);
			MPI_Wait(&request[startScope], MPI_STATUS_IGNORE);
			++startScope;
		}
	}

	MPI_Waitall(max_ping_pong, request, MPI_STATUS_IGNORE);

	free(request);
	free(status);
	free(data);
	free(results);
}

int result(int max_ping_pong, int rank, int data_size)
{
	double start = MPI_Wtime();
	MPI_Status status;

	if (rank % 2)
	{
		_mpi_loop(rank - 1, max_ping_pong, status, data_size);
	}
	else
	{
		_mpi_loop(rank + 1, max_ping_pong, status, data_size);
		double time = MPI_Wtime() - start;
		printf("\nnice, you do synchronous task in %lf\n", time);
	}

	return 0;
}

int result_async(int max_ping_pong, int rank, int data_size)
{
	double start = MPI_Wtime();

	if (rank % 2)
	{
		_mpi_loop_async(rank - 1, max_ping_pong, data_size);
	}
	else
	{
		_mpi_loop_async(rank + 1, max_ping_pong, data_size);
		double time = MPI_Wtime() - start;
		printf("\nnice, you do asynchronous task in %lf\n", time);
	}
	return 0;
}

int main(int argc, char *argv[])
{
	int max_ping_pong = strtol(argv[1], NULL, 10);
	int data_size = strtol(argv[2], NULL, 10);

	int pn;
	int rank;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &pn);
	if ((pn < 1) || (pn % 2)) {
		printf("need more than one procedure and number of procedure must be even\n");
		MPI_Finalize();
		return 1;
	}
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	result(max_ping_pong, rank, data_size);
	// result_async(max_ping_pong, rank, data_size);
		
	MPI_Finalize();
	return 0;
}
